# D&B Product Design Components

### Current Version: 0.2.3

The D&B Product Design Components were created to make your job easier! It includes everything you need to quickly recreate any of the examples found on the [Product Design Guide](http://productdesign.dnb.com/).

These components are a customization of [Bootstrap](http://getbootstrap.com) and the items provided here are intenede to be used in lieu of Bootstrap. You can follow the guidlines set by Bootstrap to create the elements and layouts you need.  Any neuances added by the D&B UXD team are noted on the Product Design Guide site with sample code snippets.

New components are being added regularly, so make sure you have the latest version.  If you need something that has not yet been added, please contact [Cassie Sharp](mailto:csharp@hoovers.com?Subject=Product%20Design%20-%20Design%20Inquiry) and we will try to get it added as quickly as possible.



## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
dnb-product-design/
├── dist/
│   ├── css/
│   │   ├── dnb-design.css
│   │   ├── dnb-design.css.map
│   │   └── dnb-design.min.css
│   └── js/
│       ├── dnb-design.js
│       └── dnb-design.min.js
├── js/
│   └── All D&B and Bootstrap js
│
│
└── less/
    └── All D&B and Bootstrap LESS
```

We provide compiled CSS and JS (`dnb-design.*`), as well as compiled and minified CSS and JS (`dnb-design.min.*`). CSS [source maps](https://developers.google.com/chrome-developer-tools/docs/css-preprocessors) (`dnb-design.*.map`) are available for use with certain browsers' developer tools.  We also provide the source code for LESS and javascript, in case you need to customize something for your specific app.

Custom icon fonts are imported into the CSS and do not require anything to set up.

* * *

Designed, coded, and maintained by the D&B UXD Team
