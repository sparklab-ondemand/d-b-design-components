/* ========================================================================
 * D&B Product Design: dnb-left-hand-menu.js v0.2.0
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  
  'use strict';
  dnbLeftHandMenu()

  // Do stuff when the window resizes
  $(window).resize(function() {
    dnbLeftHandMenu();
  });

});

function dnbLeftHandMenu() {
  var leftMenu = jQuery('.dnb-left-hand-menu');
  var mainNavbar = jQuery('.navbar-default');
  leftMenu.css('top', "-" + mainNavbar.css('margin-bottom'));
}