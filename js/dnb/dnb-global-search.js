/* ========================================================================
 * D&B Product Design: dnb-global-search.js v0.2.0
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  
  'use strict';

  // Do stuff when the window resizes
  $(window).resize(function() {
    checkGlobalSearchField();
  });

  // Set size of Global header nav search field
  checkGlobalSearchField();

  // Global header nav search field expanding

  $('.search.form-control').focus(function() {
    if ($(window).width() > 767) {
      var navBar = $(this).parents('.navbar-default');
      var searchBar = navBar.find('.navbar-form');
      var navLogoWidth = navBar.find('.navbar-brand').outerWidth();
      var searchNewWidth = navBar.width() - navLogoWidth - 40 - ($('.small-item').width() * $('.small-item').length);
      // console.log(searchNewWidth)

      searchBar.css({
        'width': searchBar.width(),
        'left': navLogoWidth  
      });
      navBar.addClass('searchFocus');
      setTimeout(function() {
        searchBar.width(searchNewWidth);
      }, 10);
    }
  });
  $('.search.form-control').blur(function() {
    setTimeout(function() {
      checkGlobalSearchField();
    }, 250);
  });
  $('.btn-search').blur(function() {
    checkGlobalSearchField();
  });

  $('.accordion-toggle').click(function() {
    var setHeight = setInterval(function() {
      $('.page-wrapper').css('min-height', $('#left-hand-menu .nav').height());
    }, 10);

    if (!$(this).parent().hasClass('open')) {
      $(this).parent().addClass('open');
      $(this).next().slideDown(400, function() {
        clearInterval(setHeight);
      });
    } else {
      $(this).next().slideUp(400, function() {
        clearInterval(setHeight);
        $(this).parent().removeClass('open');
      });
    }
  });

});

function checkGlobalSearchField() {
  var searchField = jQuery('.search.form-control');
  var navBar = searchField.parents('.navbar-default');
  var searchBar = navBar.find('.navbar-form');
  var searchDropdown = searchBar.find('.search-dropdown');
  var navLogoWidth = navBar.find('.navbar-brand').outerWidth();
  var navUlWidth = function() {
    var width = 0;
    navBar.find('li').each(function() {
      width += jQuery(this).width();
    });
    return width;
  };
  var searchOrigWidth = navBar.width() - navLogoWidth - navUlWidth() - 25 + searchDropdown.width();

  searchDropdown.width(searchDropdown.width());

  if (jQuery(window).width() < 768) {
    searchBar.width(navBar.width() - navLogoWidth - navBar.find('.navbar-toggle').width() - 35);
    searchBar.css('left', navLogoWidth);
  } else {
    if (!searchField.val()) {
      searchBar.width(searchOrigWidth);
        setTimeout(function() {
        navBar.removeClass('searchFocus');
      }, 250);
    } else if (jQuery(document.activeElement).attr('class').indexOf('btn-search') < 0) {
      searchBar.width(searchOrigWidth);
      navBar.addClass('searchHasVal');
      setTimeout(function() {
        navBar.removeClass('searchFocus');
      }, 250);
    }
  }
}
